package com.beon.androidchallenge.ui.main

import android.os.Looper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit
import java.util.logging.Handler

class MainViewModel : ViewModel() {

    val currentFact = MutableLiveData<Fact?>(null)
    val showLoading = MutableLiveData<Boolean>(false)
    val errorMessage = MutableLiveData<String>("")
    private var isWriting = false

    fun searchNumberFact(number: String) {
        if (number.isEmpty()) {
            currentFact.postValue(null)
            return
        }

        if (!isWriting) {
            showLoading.postValue(true)
            isWriting = true
            FactRepository.getInstance().getFactForNumber(number, object : FactRepository.FactRepositoryCallback<Fact> {
                override fun onResponse(response: Fact) {
                    showLoading.postValue(false)
                    currentFact.postValue(response)
                    CoroutineScope(Dispatchers.IO).launch {
                        delay(TimeUnit.SECONDS.toMillis(3))
                        isWriting = false
                    }
                }

                override fun onError() {
                    showLoading.postValue(false)
                    errorMessage.postValue("Something went wrong")
                }

            })
        }
    }
}